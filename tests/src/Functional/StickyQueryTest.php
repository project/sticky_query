<?php

declare(strict_types=1);

namespace Drupal\Tests\sticky_query\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * StickyQuery test.
 *
 * @group sticky_query
 */
final class StickyQueryTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'sticky_query',
    'sticky_query_test',
  ];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests that no StickyQuery not sticks.
   */
  public function testNoStickyQuery() {
    $url = Url::fromRoute('user.login');
    $this->drupalGet($url);
    $this->assertSession()->linkByHrefExists('#main-content');
    $this->assertSession()->linkByHrefNotExists('?test=yo#main-content');
  }

  /**
   * Tests that StickyQuery sticks.
   */
  public function testStickyQuery() {
    $url = Url::fromRoute('user.login')
      ->setOption('query', ['test' => 'yo']);
    $this->drupalGet($url);
    $this->assertSession()->linkByHrefExists('?test=yo#main-content');
  }

}
