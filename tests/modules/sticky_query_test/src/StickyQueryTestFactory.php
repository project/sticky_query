<?php

declare(strict_types=1);

namespace Drupal\sticky_query_test;

use Drupal\sticky_query\StickyQuery\StickyQueryHandlerFactoryInterface;
use Drupal\sticky_query\StickyQuery\StickyQueryHandlerRegistry;
use Drupal\sticky_query\StickyQuery\StickyQueryLinkDrivenValueHandler;
use Drupal\sticky_query\StickyQueryStorage\StickyQuerySimpleStorage;

final class StickyQueryTestFactory implements StickyQueryHandlerFactoryInterface {

  public function registerHandlers(StickyQueryHandlerRegistry $registry): void {
    $registry->add(new StickyQueryLinkDrivenValueHandler('test', new StickyQuerySimpleStorage()));
  }

}
