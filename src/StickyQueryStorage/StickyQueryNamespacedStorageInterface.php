<?php

namespace Drupal\sticky_query\StickyQueryStorage;

interface StickyQueryNamespacedStorageInterface extends StickyQueryStorageInterface {

  public function getNamespacedStorage(string $namespace): self;

}
