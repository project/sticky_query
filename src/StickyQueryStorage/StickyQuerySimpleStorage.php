<?php

namespace Drupal\sticky_query\StickyQueryStorage;

class StickyQuerySimpleStorage implements StickyQueryNamespacedStorageInterface {

  /**
   * @var mixed
   */
  protected $value = NULL;

  /**
   * @return mixed
   */
  public function getValue() {
    return $this->value;
  }

  /**
   * @param mixed $value
   */
  public function setValue($value): void {
    $this->value = $value;
  }

  public function getNamespacedStorage(string $namespace): self {
    $instance = new static();
    $instance->value =& $this->value[$namespace];
    return $instance;
  }
  
}
