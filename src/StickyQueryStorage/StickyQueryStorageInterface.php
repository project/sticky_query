<?php

namespace Drupal\sticky_query\StickyQueryStorage;

interface StickyQueryStorageInterface {

  /**
   * @return mixed
   */
  public function getValue();

  /**
   * @param mixed $value
   */
  public function setValue($value): void;

}