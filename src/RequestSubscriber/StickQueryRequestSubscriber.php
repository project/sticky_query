<?php

namespace Drupal\sticky_query\RequestSubscriber;

use Drupal\sticky_query\StickyQuery\StickyQueryHandlerFactoryCollector;
use Drupal\sticky_query\StickyQuery\StickyQueryHandlerRegistry;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class StickQueryRequestSubscriber implements EventSubscriberInterface {

  protected RequestStack $requestStack;

  protected StickyQueryHandlerRegistry $registry;

  protected StickyQueryHandlerFactoryCollector $factoryCollector;

  public function __construct(RequestStack $requestStack, StickyQueryHandlerRegistry $registry, StickyQueryHandlerFactoryCollector $factoryCollector) {
    $this->requestStack = $requestStack;
    $this->registry = $registry;
    $this->factoryCollector = $factoryCollector;
  }


  public static function getSubscribedEvents() {
    return [
      KernelEvents::REQUEST => [['onRequest', 999]],
    ];
  }

  public function onRequest(RequestEvent $event) {
    $request = $event->getRequest();
    $this->inheritQueryValuesFromParentRequest($request);
    $this->registerHandlers();
    $this->initializeHandlers($request);
  }

  private function inheritQueryValuesFromParentRequest(Request $request): void {
    if (!$request === $this->requestStack->getCurrentRequest()) {
      throw new \LogicException("Request mismatch: $request / {$this->requestStack->getCurrentRequest()}");
    }
    if ($parentRequest = $this->getParentRequest()) {
      foreach ($this->registry->getHandlers($parentRequest) as $handler) {
        $key = $handler->getKey();
        $value = $handler->mergeSubRequestValue($request->query->get($key));
        if (isset($value)) {
          $request->query->set($key, $value);
        }
        else {
          $request->query->remove($key);
        }
      }
    }
  }

  private function registerHandlers(): void {
    foreach ($this->factoryCollector->getFactories() as $factory) {
      $factory->registerHandlers($this->registry);
    }
  }

  private function initializeHandlers(Request $request): void {
    foreach ($this->registry->getHandlers() as $handler) {
      $value = $request->query->get($handler->getKey());
      $handler->storeInboundValue($value);
    }
  }

  private function getParentRequest(): ?Request {
    // Work around a core bug that pushes requests twice.
    $requestStack = clone($this->requestStack);
    $current = $requestStack->pop();
    do {
      $parent = $requestStack->pop();
    } while ($current === $parent);
    return $parent;
  }

}
