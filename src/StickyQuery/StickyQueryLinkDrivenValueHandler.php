<?php

namespace Drupal\sticky_query\StickyQuery;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;

/**
 * Trait for link driven sticky query.
 *
 * Link-driven means that the application adds our query component to some links
 * and this is intended behavior.
 * See role_toggle.module, where such links are used to switch roles.
 *
 * In this case, incoming links usually do not carry our query component, and
 * the query component is added.
 * One challenge arises: Hot to remove the query component and get an outbound
 * link without it?
 * In this case we use an empty string to signal "remove me".
 * But any other magic value will do, too.
 *
 * ALso note:
 * - UrlHelper::buildQuery(['foo'=>NULL, 'bar'=> '']) === 'foo&bar='
 * - parse_str('foo&bar') === ['foo'=>'', 'bar'=> '']
 *
 * This class has intentionally no methods to alter the stored inbound values.
 * Normally, all altering happens by providing links that change state.
 * But there is another case: If internal app state  changes (in the above case,
 * roles are added / removed), ::fetchOutboundValue may return a value different
 * from that passed to ::storeInboundValue.
 * In the role_toggle c ase, such changes can only happen on submit of a role
 * entity form, the redirect URL of which is always not cacheable.
 *
 * In the general case this means: Any subclass must guarantee that
 * - either ::fetchOutboundValue(::storeInboundValue) == ID
 * - or current response is uncacheable (e.g. form submit)
 */
class StickyQueryLinkDrivenValueHandler extends StickyQueryHandlerBase {

  public function mergeOutboundValue($hrefValue, RefinableCacheableDependencyInterface &$cacheability) {
    if ($this->isMagicRemoveMeValue($hrefValue)) {
      // NULL means, remove query component.
      return NULL;
    }
    else {
      // Any explicitly set value is stronger that the set value.
      return $hrefValue ?? $this->storage->getValue();
    }
  }

  /**
   * @param mixed $hrefValue
   */
  protected function isMagicRemoveMeValue($hrefValue): bool {
    // Empty string means "url/path?key=" or "url/path?key"
    return $hrefValue === '';
  }

}
