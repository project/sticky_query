<?php

namespace Drupal\sticky_query\StickyQuery;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;

/**
 * Sticky Query Handler for App Driven Values
 *
 * E.g. the sessionless module adds a session handler that, instead of PHP
 * sessions, uses a sticky query value as storage, made tamper-proof via
 * cryptor signatures, optionally encrypted.
 *
 * Let's look at cacheability.
 * In the default case, all outbound links sticky query reflect the current
 * state, which reflect the inbound sticky query, and is covered by the
 * url.query cache context.
 * When state and sticky query changes or may change, the response must be
 * uncacheable. Usually this happens on form submit, which returns an
 * uncacheable redirect.
 * Any subclass of this class must guarantee that state change only happens in
 * this constellation.
 */
class StickyQueryAppDrivenValueHandler extends StickyQueryHandlerBase {

  public function mergeOutboundValue($hrefValue, RefinableCacheableDependencyInterface &$cacheability) {
    // In this implementation, the set value is stronger than the href value.
    return $this->storage->getValue();
  }

}
