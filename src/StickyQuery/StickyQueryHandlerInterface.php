<?php

namespace Drupal\sticky_query\StickyQuery;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;

interface StickyQueryHandlerInterface extends RefinableCacheableDependencyInterface {

  public function getKey(): string;

  /**
   * @param mixed $value
   */
  public function storeInboundValue($value): void;

  /**
   * @param mixed $hrefValue
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $cacheability
   *
   * @return mixed
   */
  public function mergeOutboundValue($hrefValue, RefinableCacheableDependencyInterface &$cacheability);

  /**
   * @param mixed $hrefValue
   *
   * @return mixed
   */
  public function mergeSubRequestValue($hrefValue);

}
