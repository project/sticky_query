<?php

namespace Drupal\sticky_query\StickyQuery;

class StickyQueryHandlerFactoryCollector {

  protected StickyQueryHandlerRegistry $registry;

  /**
   * @var array<\Drupal\sticky_query\StickyQuery\StickyQueryHandlerFactoryInterface>
   */
  protected array $factories = [];

  public function __construct(StickyQueryHandlerRegistry $registry) {
    $this->registry = $registry;
  }

  public function addFactory(StickyQueryHandlerFactoryInterface $factory): void {
    $this->factories[] = $factory;
  }

  /**
   * @return \Drupal\sticky_query\StickyQuery\StickyQueryHandlerFactoryInterface[]
   */
  public function getFactories(): array {
    return $this->factories;
  }

}
