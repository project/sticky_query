<?php

namespace Drupal\sticky_query\StickyQuery;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;

class StickyQueryHandlerRegistry {

  protected RequestStack $requestStack;

  protected \SplObjectStorage $handlersByRequest;

  /**
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   */
  public function __construct(RequestStack $requestStack) {
    $this->requestStack = $requestStack;
    $this->handlersByRequest = new \SplObjectStorage();
  }

  /**
   * @return \Drupal\sticky_query\StickyQuery\StickyQueryHandlerInterface[]
   */
  public function getHandlers(Request $request = NULL): array {
    $request = $request ?? $this->requestStack->getCurrentRequest();
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $handlers = $this->handlersByRequest[$request] ?? [];
    return $handlers;
  }

  public function add(StickyQueryHandlerInterface $handler, Request $request = NULL) {
    $request = $request ?? $this->requestStack->getCurrentRequest();
    // @todo Cough on collisions.
    $handlers = $this->handlersByRequest[$request] ?? [];
    $key = $handler->getKey();
    $handlers[$key] = $handler;
    $this->handlersByRequest[$request] = $handlers;
  }

}
