<?php

namespace Drupal\sticky_query\StickyQuery;

interface StickyQueryHandlerFactoryInterface {

  public function registerHandlers(StickyQueryHandlerRegistry $registry): void;

}
