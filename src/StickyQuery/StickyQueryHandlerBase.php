<?php

namespace Drupal\sticky_query\StickyQuery;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\RefinableCacheableDependencyTrait;
use Drupal\sticky_query\StickyQueryStorage\StickyQueryStorageInterface;

abstract class StickyQueryHandlerBase implements StickyQueryHandlerInterface {

  use RefinableCacheableDependencyTrait;

  protected string $key;

  protected StickyQueryStorageInterface $storage;

  public function __construct(string $key, StickyQueryStorageInterface $storage) {
    $this->key = $key;
    $this->storage = $storage;
  }

  public function getKey(): string {
    return $this->key;
  }

  public function storeInboundValue($value): void {
    $this->storage->setValue($value);
  }

  public function mergeSubRequestValue($hrefValue) {
    // Default to use outbound value.
    $cacheability = new CacheableMetadata();
    return $this->mergeOutboundValue($hrefValue, $cacheability);
  }

}
