<?php

namespace Drupal\sticky_query\UrlProcessor;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\Routing\RequestContext;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\domprocessor\UrlProcessor\UrlProcessorInterface;
use Drupal\sticky_query\StickyQuery\StickyQueryHandlerRegistry;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class QueryProcessorBase
 */
final class StickyQueryUrlProcessor implements UrlProcessorInterface {

  protected RequestStack $requestStack;

  protected StickyQueryHandlerRegistry $registry;

  protected RequestContext $requestContext;

  public function __construct(RequestStack $requestStack, StickyQueryHandlerRegistry $registry, RequestContext $requestContext) {
    $this->requestStack = $requestStack;
    $this->registry = $registry;
    $this->requestContext = $requestContext;
  }

  public function applies(HtmlResponse $response): bool {
    return TRUE;
  }

  public function processUrl(\DomAttr $node, HtmlResponse $response): void {
    $node->value = htmlspecialchars($this->processUrlTarget(htmlspecialchars_decode($node->value), $response));
  }

  public function processRedirectResponse(RedirectResponse $response): void {
    $url = $response->getTargetUrl();
    $url = $this->processUrlTarget($url, $response);
    $response->setTargetUrl($url);
  }

  private function processUrlTarget(string $url, Response $response): string {
    $urlParts = parse_url($url);
    if (!empty($urlParts['scheme']) && !in_array($urlParts['scheme'], ['http', 'https'])) {
      // No need to process non-http URLs.
      // Also, UrlHelper::externalIsLocal coughs on e.g. mailto: URLs.
      return $url;
    }
    if (!$this->isLocal($url)) {
      return $url;
    }
    $parsed = UrlHelper::parse($url);
    $query =& $parsed['query'];
    $originalQuery = $query;
    foreach ($this->registry->getHandlers() as $handler) {
      $key = $handler->getKey();
      $cacheability = (new CacheableMetadata())
        ->addCacheContexts(["url:query:{$key}"]);
      $outboundValue = $handler->mergeOutboundValue($query[$key] ?? NULL, $cacheability);
      if (isset($outboundValue)) {
        $query[$key] = $outboundValue;
      }
      else {
        unset($query[$key]);
      }
      if ($response instanceof CacheableResponseInterface) {
        $response->addCacheableDependency($handler);
      }
    }
    if ($query !== $originalQuery) {
      $parsed['query'] = UrlHelper::buildQuery($query);
      $url = $this->buildUrl($parsed);
    }
    return $url;
  }

  protected function buildUrl(array $parsed): string {
    return ($parsed['path'] ?? '')
      . ($parsed['query'] ? '?' . $parsed['query'] : '')
      . ($parsed['fragment'] ? '#' . $parsed['fragment'] : '');
  }

  protected function isLocal(string $url): bool {
    // Copied from \Drupal\Core\Routing\LocalAwareRedirectResponseTrait::isLocal
    return !UrlHelper::isExternal($url) || UrlHelper::externalIsLocal($url, $this->requestContext->getCompleteBaseUrl());
  }

}
